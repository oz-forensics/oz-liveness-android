# Sample application for Oz Liveness SDK

[SDK documentation](https://oz-forensics.gitlab.io/oz-liveness-android/).  
[SDK change log](CHANGELOG.md).

## **Requirements**:

Minimal Android SDK version: 21.

Also consider aligning your build tools versions with the table below, so we can offer better support for these:

| Gradle Version | Kotlin Version | AGP Version | Java Target Level | JDK Version |
|:--------------:|:--------------:|:-----------:|:-----------------:|:-----------:|
| 7.5.1          | 1.7.21         | 7.3.1       | 1.8               | 17          |

## **To start using Oz Android SDK, follow the steps below.**

### 1\. Get a trial license for SDK on our website or a production license by email. We'll need your application id. Add the license to your project:

To pass your license file to the SDK, call the OzLivenessSDK.init method with a list of LicenseSources. Use one of the following:
LicenseSource.LicenseAssetId should contain a path to a license file called forensics.license, which has to be located in the project's res/raw folder.
LicenseSource.LicenseFilePath should contain a file path to the place in the device's storage where the license file is located.

```kotlin
OzLivenessSDK.init(context,
    listOf(
        LicenseSource.LicenseAssetId(R.raw.your_license_name),
        LicenseSource.LicenseFilePath("absolute_path_to_your_license_file")
    ),
    object : StatusListener<LicensePayload> {
        override fun onSuccess(result: LicensePayload) { /*check the license payload*/ }
        override fun onError(error: OzException) { /*handle the exception */ }
    }
  )
```

### 2\. Embed Oz Android SDK into your project:

Add in top-level project `build.gradle`:

```groovy
allprojects {
    repositories {
        maven { url 'https://ozforensics.jfrog.io/artifactory/main' }
    }
}
```

- Add in module `build.gradle`:

```groovy
dependencies {
    implementation 'com.ozforensics.liveness:sdk:VERSION'
}
```

- Add in module `build.gradle`:

 ```groovy
android {
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
}
```
### 3\. Connect SDK to API.

> This step is optional, as this connection is required only when you need to process data on a server. If you use the on-device mode, the data is not transferred anywhere, and no connection is needed.

To connect SDK to Oz API, you can use the OzLivenessSDK.setApiConnection method with host and access token as shown below.

```kotlin
OzLivenessSDK.setApiConnection(
    OzConnection.fromServiceToken(serverUrl, token),
    statusListener(
        { /* successfully login */ },
        { error -> /* error while login */ }
    )
)
```
> **Please note:** in your host application, it is recommended that you set the API address on the screen that precedes the liveness check. Setting the API URL initiates a service call to the API, which may cause excessive server load when being done at the application initialization or startup.


Alternatively, you can use the OzLivenessSDK.setApiConnection method with host, login and password provided by your Oz Forensics account manager:

```kotlin
OzLivenessSDK.setApiConnection(
    OzConnection.fromCredentials(serverUrl, username, password),
    statusListener(
        { /* successfully login */ },
        { error -> /* error while login */ }
    )
)
```
Although, the preferred option is authentication via access token – for security reasons.

### 4\. Capture videos using methods described [here](https://ozforensics.atlassian.net/oz-knowledge/guides/developer-guide/sdk/oz-mobile-sdk/android/capturing-videos). You'll send them for analysis afterward.

### 5\. Analyze media you've taken at the previous step. The process of checking liveness and face biometry is described [here](https://ozforensics.atlassian.net/oz-knowledge/guides/developer-guide/sdk/oz-mobile-sdk/android/checking-liveness-and-face-biometry).

### 6\. If you want to customize the look-and-feel of Oz Android SDK, please refer to [this section](https://ozforensics.atlassian.net/oz-knowledge/guides/developer-guide/sdk/oz-mobile-sdk/android/customizing-android-sdk).