package com.ozforensics.liveness.sample

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.ozforensics.liveness.sample.databinding.ActivityLoginBinding
import com.ozforensics.liveness.sdk.core.OzLivenessSDK
import com.ozforensics.liveness.sdk.core.StatusListener
import com.ozforensics.liveness.sdk.exceptions.OzException
import com.ozforensics.liveness.sdk.network.OzConnection
import com.ozforensics.liveness.sdk.security.LicenseSource

class LoginActivity : AppCompatActivity() {

    private var binding: ActivityLoginBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        OzLivenessSDK.init(this, listOf(LicenseSource.LicenseAssetId(R.raw.forensics)))
        binding = ActivityLoginBinding.inflate(layoutInflater).apply {
            loginButton.setOnClickListener { login() }
            setContentView(root)
        }
    }

    private fun ActivityLoginBinding.login() {
        startProgress()
        val serverUrl = inputUrl.text.toString().trim()
        val username = inputEmail.text.toString().trim()
        val password = inputPassword.text.toString().trim()

        OzLivenessSDK.setApiConnection(
            OzConnection.fromCredentials(serverUrl, username, password),
            object : StatusListener<String?> {
                override fun onSuccess(result: String?) {
                    resultAlertDialog(
                        R.string.login_activity_dialog_auth_success,
                        "",
                        true
                    )
                }

                override fun onError(error: OzException) {
                    resultAlertDialog(
                        R.string.login_activity_auth_error,
                        "[${error.message}]",
                        false
                    )
                }
            }
        )
    }

    private fun resultAlertDialog(idMessage: Int, details: String = "", success: Boolean) {
        stopProgress()
        AlertDialog.Builder(this)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                if (success) {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }
            }
            .setMessage(getString(idMessage) + "\n" + details)
            .show()
    }

    private fun startProgress() {
        binding?.progressBarLogin?.visibility = View.VISIBLE
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    private fun stopProgress() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        binding?.progressBarLogin?.visibility = View.GONE
    }
}
