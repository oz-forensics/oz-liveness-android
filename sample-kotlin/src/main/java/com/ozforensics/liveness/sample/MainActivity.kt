package com.ozforensics.liveness.sample

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ozforensics.liveness.sample.databinding.ActivityMainBinding
import com.ozforensics.liveness.sdk.analysis.AnalysisRequest
import com.ozforensics.liveness.sdk.analysis.entity.Analysis
import com.ozforensics.liveness.sdk.core.Cancelable
import com.ozforensics.liveness.sdk.core.OzLivenessResultCode
import com.ozforensics.liveness.sdk.core.OzLivenessSDK
import com.ozforensics.liveness.sdk.core.model.*
import com.ozforensics.liveness.sdk.customization.*

class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding? = null
    private var analysisCancelable: Cancelable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater).apply {
            buttonStart.setOnClickListener {
                val actions = listOf(OzAction.Smile, OzAction.Scan)
                val intent = OzLivenessSDK.createStartIntent(actions)
                startActivityForResult(intent, REQUEST_CODE_SDK)
            }
            setContentView(root)
        }
        val customization = OzLivenessSDK.config.customization
        with(customization.toolbarCustomization) {
            titleText = "Kotlin Sample"
        }
        OzLivenessSDK.config.allowDebugVisualization = true
        OzLivenessSDK.config.attemptSettings = OzAttemptsSettings(2, 3)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
        analysisCancelable?.cancel()
        analysisCancelable = null
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SDK) {
            when (resultCode) {
                OzLivenessResultCode.SUCCESS -> analyzeMedia(OzLivenessSDK.getResultFromIntent(data))
                OzLivenessResultCode.USER_CLOSED_LIVENESS -> { /* user closed the screen */ }
                else -> OzLivenessSDK.getErrorFromIntent(data)?.let { showHint(it) }
            }
        }
    }

    private fun analyzeMedia(mediaList: List<OzAbstractMedia>?) {
        if (mediaList.isNullOrEmpty()) return
        analysisCancelable?.cancel()
        analysisCancelable = AnalysisRequest.Builder()
            .addAnalysis(Analysis(Analysis.Type.QUALITY, Analysis.Mode.SERVER_BASED, mediaList))
            .build()
            .run(
                { result ->
                    val resultString = result.analysisResults.joinToString(separator = "/n") { it.type.name + " - " + it.resolution }
                    showHint(resultString)
                },
                { showHint(it.message ?: "Unknown error") },
                { showHint(it.toString()) },
            )
    }

    private fun showHint(text: String) {
        binding?.textViewHint?.text = text
    }

    companion object {
        private const val REQUEST_CODE_SDK = 5
    }
}
