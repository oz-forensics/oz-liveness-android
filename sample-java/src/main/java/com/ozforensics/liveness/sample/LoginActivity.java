package com.ozforensics.liveness.sample;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.ozforensics.liveness.sample.databinding.ActivityLoginBinding;
import com.ozforensics.liveness.sdk.core.Cancelable;
import com.ozforensics.liveness.sdk.core.OzLivenessSDK;
import com.ozforensics.liveness.sdk.core.StatusListener;
import com.ozforensics.liveness.sdk.exceptions.OzException;
import com.ozforensics.liveness.sdk.network.OzConnection;
import com.ozforensics.liveness.sdk.security.LicenseSource;

import java.util.Collections;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    ActivityLoginBinding activityLoginBinding = null;
    Cancelable loginCancelable = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        OzLivenessSDK.init(
            this,
            Collections.singletonList(new LicenseSource.LicenseAssetId(R.raw.forensics))
        );
        if (OzLivenessSDK.isLoggedIn()) launchMainActivity();

        super.onCreate(savedInstanceState);
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        activityLoginBinding.btnLogin.setOnClickListener(view -> {
            if (loginCancelable != null) loginCancelable.cancel();
            String login = Objects.requireNonNull(activityLoginBinding.etLogin.getText()).toString();
            String password = Objects.requireNonNull(activityLoginBinding.etPassword.getText()).toString();
            String baseUrl = Objects.requireNonNull(activityLoginBinding.etUrl.getText()).toString();

            if (Patterns.WEB_URL.matcher(baseUrl).matches()
                    && Patterns.EMAIL_ADDRESS.matcher(login).matches()
                    && !password.isEmpty()
            ) {
                activityLoginBinding.pbLoading.setVisibility(View.VISIBLE);
                loginCancelable = OzLivenessSDK.setApiConnection(
                    OzConnection.fromCredentials(baseUrl, login, password),
                    new StatusListener<String>() {
                        @Override
                        public void onStatusChanged(@Nullable String s) {}
                        @Override
                        public void onSuccess(String token) {
                            activityLoginBinding.pbLoading.setVisibility(View.GONE);
                            loginCancelable = null;
                            Toast.makeText(getApplicationContext(), "Authorization success", Toast.LENGTH_SHORT).show();
                            launchMainActivity();
                        }

                        @Override
                        public void onError(@NonNull OzException e) {
                            activityLoginBinding.pbLoading.setVisibility(View.GONE);
                            loginCancelable = null;
                            Toast.makeText(getApplicationContext(), "Authorization failed:" + e, Toast.LENGTH_SHORT).show();
                        }
                    }
                );
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activityLoginBinding = null;
        if (loginCancelable != null) {
            loginCancelable.cancel();
            loginCancelable = null;
        }
    }

    private void launchMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
