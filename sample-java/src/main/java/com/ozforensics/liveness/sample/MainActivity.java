package com.ozforensics.liveness.sample;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.ozforensics.liveness.sample.databinding.ActivityMainBinding;
import com.ozforensics.liveness.sdk.analysis.AnalysisRequest;
import com.ozforensics.liveness.sdk.analysis.entity.Analysis;
import com.ozforensics.liveness.sdk.analysis.entity.RequestResult;
import com.ozforensics.liveness.sdk.core.Cancelable;
import com.ozforensics.liveness.sdk.core.OzLivenessResultCode;
import com.ozforensics.liveness.sdk.core.OzLivenessSDK;
import com.ozforensics.liveness.sdk.core.model.OzAbstractMedia;
import com.ozforensics.liveness.sdk.core.model.OzAction;
import com.ozforensics.liveness.sdk.customization.UICustomization;
import com.ozforensics.liveness.sdk.exceptions.OzException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.*;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding activityMainBinding = null;
    Cancelable analysisCancelable = null;
    int REQUEST_CODE_LIVENESS = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        applyCustomization();
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activityMainBinding.btnLogout.setOnClickListener((view) -> {
            OzLivenessSDK.logout();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        });
        activityMainBinding.btnStart.setOnClickListener((view) -> {
            new AlertDialog.Builder(this)
                .setItems(Arrays.stream(OzAction.values()).map((action) -> (CharSequence) action.name()).toArray(CharSequence[]::new), (dialogInterface, i) -> {
                    Intent intent = OzLivenessSDK.createStartIntent(Collections.singletonList(OzAction.values()[i]));
                    startActivityForResult(intent, REQUEST_CODE_LIVENESS);
                }).show();
        });
    }

    private void applyCustomization() {
        UICustomization uiCustomization = new UICustomization();
        uiCustomization.getToolbarCustomization().setTitleText("Java Sample");
        OzLivenessSDK.config.setCustomization(uiCustomization);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activityMainBinding = null;
        if (analysisCancelable != null) {
            analysisCancelable.cancel();
            analysisCancelable = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_LIVENESS && data != null) {
            if (resultCode == OzLivenessResultCode.SUCCESS) {
                List<OzAbstractMedia> media = OzLivenessSDK.getResultFromIntent(data);
                if (media != null && !media.isEmpty()) analyzeMedia(media);
                else showStatus("Empty list");
            } else  {
                String errorMessage = OzLivenessSDK.getErrorFromIntent(data);
                showStatus("Error: " + errorMessage);
            }
        }
    }

    private void analyzeMedia(List<OzAbstractMedia> mediaList) {
        if (analysisCancelable != null) analysisCancelable.cancel();
        showProgress(true);
        analysisCancelable = new AnalysisRequest.Builder()
                .addAnalysis(new Analysis(Analysis.Type.QUALITY, Analysis.Mode.SERVER_BASED, mediaList, Collections.emptyMap()))
                .build()
                .run(new AnalysisRequest.AnalysisListener() {
                    @Override
                    public void onStatusChange(@NonNull AnalysisRequest.AnalysisStatus analysisStatus) { showStatus(analysisStatus.toString()); }
                    @Override
                    public void onSuccess(@NonNull RequestResult result) {
                        String message = result.getAnalysisResults().stream().map((res) -> "Analysis " + res.getType() + " - " + result.getResolution().name()).collect(Collectors.joining("\n"));
                        showStatus(message);
                        showProgress(false);
                    }
                    @Override
                    public void onError(@NonNull OzException e) {
                        showStatus("Error: " + e);
                        showProgress(false);
                    }
                });
    }

    private void showStatus(String errorMessage) {
        activityMainBinding.tvStatus.setText(errorMessage);
    }

    private void showProgress(boolean isProgress) {
        activityMainBinding.pbLoading.setVisibility(isProgress ? View.VISIBLE : View.GONE);
        activityMainBinding.btnStart.setVisibility(isProgress ? View.GONE : View.VISIBLE);
        activityMainBinding.btnLogout.setVisibility(isProgress ? View.GONE : View.VISIBLE);
    }
}
