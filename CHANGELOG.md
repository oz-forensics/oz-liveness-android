# Change Log

Starting from 6.3.5, you can find the changelog [here](https://doc.ozforensics.com/oz-knowledge/other/changelog/android-sdk)

## Version 6.3.5 (2022-03-04)

* Added a new simplified API – AnalysisRequest. With it, it’s easier to create a request for the media and analysis you need. [Here's an example](https://doc.ozforensics.com/oz-knowledge/guides/developer-guide/sdk/oz-mobile-sdk/android/analysisrequest) of AnalysisRequest usage.

## Version 6.3.4 (2022-01-13)

* Published the on-device module for on-device liveness and biometry analyses.
In order to use them, add this module to your project:
```groovy
    implementation 'com.ozforensics.liveness:on-device:6.3.4'
```
* And use runOnDeviceBiometryAnalysis and runOnDeviceLivenessAnalysis methods from the OzLivenessSDK class
```kotlin
val mediaList: List<OzAbstractMedia> = ...
val biometryAnalysisResult: OzAnalysisResult = OzLivenessSDK.runOnDeviceBiometryAnalysis(mediaList)
val livenessAnalysisResult: OzAnalysisResult = OzLivenessSDK.runOnDeviceLivenessAnalysis(mediaList)
```

## Version 6.3.3 (2021-12-29)

* Liveness goes smoother
* Fixed freezes on Xiaomi devices
* Optimized image converting

## Version 6.3.2 (2021-12-07)

* Internal fixes

## Version 6.3.1 (2021-11-24)

* Added a 'metadata' parameter for OzLivenessSDK.uploadMedia and OzLivenessSDK.uploadMediaAndAnalyze methods to pass a metadata for folders
```kotlin
OzLivenessSDK.uploadMedia(
    mediaToUpload,
    statusListener,
    metadata = mapOf("debug_type" to "biometry_check")
)
```

## Version 6.2.8 (2021-11-12)

* Added a function to initialize an SDK with license sources. There are two types of LicenseSources:
LicenseSource.LicenseAssetId and LicenseSource.LicenseFilePath
 
* For initialization you should call an init method from OzLivenessSDK with a list of LicenseSources and StatusListener<LicensePayload>:
```kotlin
OzLivenessSDK.init(
    listOf(
        LicenseSource.LicenseAssetId(R.raw.your_license_name),
        LicenseSource.LicenseFilePath("absolute_path_to_your_license_file")
    ),
    object : StatusListener<LicensePayload> {
        override fun onSuccess(result: LicensePayload) { /*check the license payload*/ }
        override fun onError(error: OzException) { /*do smth with the exception */ }
    }
  )
```
* Added a function to get a license payload in runtime
```kotlin
val licensePayload = OzLivenessSDK.getLicensePayload()
```

## Version 6.2.0 (2021-09-03)

* Added functions for local analyzes.
* Added shape configuration for frame around face.
* Fixed visibility of version name on liveness screen

## Version 6.1.0 (2021-08-16)

* Added support for main camera.
* Fixed some issues.

## Version 6.0.1 (2021-08-06)

* Added configuration from license support.
* Fixed some issues.

## Version 6.0.0 (2021-07-08)

* Added `OneShot` action.
* Added more states to `OzAnalysisResult.Resolution`.
* Added overloaded `uploadMediaAndAnalyze` which takes a list of required analyzes.
* Reworked `OzMedia` into `OzAbstractMedia` and its subclasses.
* Fixed camera errors on some devices.

## Version 5.1.1 (2021-05-31)

* Fixed some issues.

## Version 5.1.0 (2021-05-25)

* Added automatic access token refresh.
* Renamed `accessToken` to `permanentAccessToken`.
* Added R8 rules.

* Simplified configuration. Now `config` properties are mutable.

  ```kotlin
  // Old configuration format
  //OzLivenessSDK.config = OzConfig.Builder(BASE_URL)
  //    .setLicenseResourceId(R.raw.license)
  //    .build()
  // New configuration format
  OzLivenessSDK.config.baseURL = BASE_URL
  OzLivenessSDK.config.licenseResourceId = R.raw.license
  ```

## Version 5.0.2 (2021-04-08)

* Fixed oval draw.
* Removed unused parameter `params` from `AnalyseRequest`.
* Removed default attempt limits.

## Version 4.4.14 (2021-04-08)

* Fixed oval draw.
* Removed unused parameter `params` from `AnalyseRequest`.
* Removed default attempt limits.

## Version 5.0.1 (2021-04-01)

* Improved image analyzer performance.
* Fixed camera errors on some devices.

## Version 4.4.13 (2021-04-01)

* Improved image analyzer performance.
* Fixed camera errors on some devices.

## Version 5.0.0 (2021-03-05)

* Configuration properties, such as `baseURL`, `accessToken`, and so on, are deprecated. Use `config` property instead, initialized by `OzConfig.Builder`:

  ```kotlin
  OzLivenessSDK.config = OzConfig.Builder(BASE_URL)
      .setAccessToken(ACCESS_TOKEN)
      .build()
  ```

  Configuration can be modified using `modify` method:

  ```kotlin
  OzLivenessSDK.config = OzLivenessSDK.config.modify()
      /* Insert your modifications here */
      .build()
  ```

* Added support for licenses. Licenses are installed as `raw` resources, and then passed into `OzConfig` using `setLicenseResourceId`:

  ```kotlin
  OzLivenessSDK.config = OzLiveness.config.modify()
      .setLicenseResourceId(R.raw.forensics)
      .build()
  ```

* Methods which required context are replaced with counterparts which don't require it. Old methods are deprecated.

  ```kotlin
  //val intent = OzLivenessSDK.createStartIntent(context, actions)
  val intent = OzLivenessSDK.createStartIntent(actions)
  ```

* Improved image analyzer performance.
* Removed unnecessary dependencies.
* Fixed errors in logging.

## Version 4.4.12 (2021-03-05)

* Improved image analyzer performance.
* Removed unnecessary dependencies.
* Fixed errors in logging.
